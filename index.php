<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="Configuration/Stylesheets/registration/RegistrationQueries.css">
	<title>Registration Page</title>
</head>
<body>
	<form class="Registration" id="formIdForUserRequest" enctype="multipart/form-data">
		<img src="Configuration/Images/Backgrounds/website-color-schemes.png" id="tempProfilePicture">
		<div class="RegistrationDetails">
			<div class="inputBlock">
				<img src="Configuration/Images/Resources/icons8-lock-64.png">
				<input type="search" name="username" id="userId" placeholder="Username" required autocomplete="off">
			</div>
			<!-- Username Block ^^ -->
			<p>Hi Enter your username above. If the <b>color changes to red</b></p>

			<div class="configurations">
				<input type="color" value="#FA8072" id="bgColor" name="colorScheme">
				<input type="color" value="#FF0000" disabled>
				<input type="color" value="#FFD700" disabled>
				<input type="color" value="#7FFF00" disabled>
				<input type="color" value="#FFD700" disabled>
				<p class="border" id="userInitials">G</p>
			</div>
			<!-- Configuration options ^^ -->
			<input type="file" name="Screenshot" id="Screenshot">
			<!-- Profile Picture ^^ -->

			<div class="inputBlock">
				<img src="Configuration/Images/Resources/icons8-lock-64.png">
				<input type="password" name="password" placeholder="Passcode" id="key">
			</div>
			<input type="file" min="0" max="50" disabled></br>
			<button id="create" onclick="RegisterSession()">Register / Sign In</button>
			<!--<input type="range" min="0" max="50" disabled>-->
		</div>
		<!-- End RegistrationDetails ^^ -->
	</form>

	<script src="Configuration/Saas/ScriptsJs/registrationQueries/validateAccountExistence.js"></script>
	<script src="Configuration/Saas/ScriptsJs/registrationQueries/uploadProfilePicture.js"></script>
</body>
</html>
