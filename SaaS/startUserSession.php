<?php
	$passcode = $_POST["password"];
	$colorScheme = $_POST["colorScheme"];
	$username = ucwords($_POST["username"]);

	// Screenshot: for the profile picture to use
	$basename = basename($_FILES["Screenshot"]["name"]);
	$sizeOfUploadedFile = $_FILES["Screenshot"]["size"];
	$tempLocationForUploadedFile = $_FILES["Screenshot"]["tmp_name"];

	// The account already exist -- FILES PRESENT
	$accountAlreadyExist = file_exists("../Accounts/Members/$username");

	if($accountAlreadyExist) {
		$savedPassword = file_get_contents("../Accounts/Members/$username/Init/passcode");
		if ($savedPassword == $passcode) {
			// Update profile picture -- if present
			if (!empty($basename)) {
				$rmScreenshot = file_get_contents("../Accounts/Members/$username/Init/Screenshot");
				if(!empty($rmScreenshot)) {
					unlink("../Accounts/Members/$username/Init/$rmScreenshot");
				}
				file_put_contents("../Accounts/Members/$username/Init/Screenshot", $basename);
				move_uploaded_file($tempLocationForUploadedFile, "../Accounts/Members/$username/Init/$basename");
			}
			// Update default color
			if ($colorScheme != "#fa8072") {
				file_put_contents("../Accounts/Members/$username/Init/ColorScheme", $colorScheme);
			}
			//return true -- username
			echo $username; 
		} else {
			echo "false";
		}
		exit();
	}

	// create the shared folder for all attachments across accounts'
	if (file_exists("../Accounts/SharedAttachmentSent") != true) {
		mkdir("../Accounts/SharedAttachmentSent");
		mkdir("../Accounts/SharedAttachmentSent/PictureFolder");
	}

	// Create account (default folders)
	mkdir("../Accounts/Members/$username");
	mkdir("../Accounts/Members/$username/Saas");
	mkdir("../Accounts/Members/$username/Init");
	mkdir("../Accounts/Members/$username/Documents");
	mkdir("../Accounts/Members/$username/Init/ColorSchemePalette");
	
	// Set the default color scheme
	file_put_contents("../Accounts/Members/$username/Init/ColorSchemePalette/cacheColorForTextBox", "#F5F5F5");
	file_put_contents("../Accounts/Members/$username/Init/ColorSchemePalette/messageHeader", "#F5F5F5");
	file_put_contents("../Accounts/Members/$username/Init/ColorSchemePalette/resourceSearchBar", "#F5F5F5");
	file_put_contents("../Accounts/Members/$username/Init/ColorSchemePalette/friendsPaneColorScheme", "#FFFFFF");
	
	// Set the default message tone
	file_put_contents("../Accounts/Members/$username/Init/MessageDeliveryTone", "Short.mp3");

	// Load all the php scripts needed into the user's folder
	$SaaS = scandir("SaaS");
	unset($SaaS[0]); // .
	unset($SaaS[1]); // ..

	foreach($SaaS as $script) {
		// Skip .DS_Store if exist -- issue for not importing ERROR_LOG.php before
		if ($script === ".DS_Store") continue;

		// Copy Scripts from SaaS Dir
		$ScriptUrl = "SaaS/$script";
		copy($ScriptUrl, "../Accounts/Members/$username/Saas/$script");
	}

	// import the default PDF's (SaaS/Documents folder)
	$Documents = scandir("Documents");
	unset($Documents[0]); // .
	unset($Documents[1]); // ..

	foreach($Documents as $DEFAULT_PDF) {
		// Skip .DS_Store if exist -- issue for not importing ERROR_LOG.php before
		if ($DEFAULT_PDF === ".DS_Store") continue;

		// Copy PDF from Documents Dir
		$DEFAULT_PDF_URL = "Documents/$DEFAULT_PDF";
		copy($DEFAULT_PDF_URL, "../Accounts/Members/$username/Documents/$DEFAULT_PDF");
	}

	// Friends
	// Take all the existing accounts on the server and create a friend 
	// Folder for all of them, so that you can automatically start messaging
	// Create an account for yourself (user) inside of all the friends you import
	$Subscribers = scandir("../Accounts/Members");
	unset($Subscribers[0]); // .
	unset($Subscribers[1]); // ..

	// Path to each friends Directory inside of your own folder
	$receiverUrl = "../Accounts/Members/$username/Friends";
	$receiverUrlInitDirUrl = "$receiverUrl/init";
	if (file_exists($receiverUrl) != true) mkdir($receiverUrl);
	if (file_exists($receiverUrlInitDirUrl) != true) mkdir($receiverUrlInitDirUrl);

	// Importing (create a link) to all the existing accounts 
	foreach($Subscribers as $subscriber) {
		if ($subscriber == ".DS_Store") continue;

		mkdir("$receiverUrl/$subscriber");
		mkdir("$receiverUrl/$subscriber/Photos");
		mkdir("$receiverUrl/$subscriber/Messages");
		mkdir("$receiverUrl/$subscriber/MemoryUtilityMetrics");
		// Create ^^
		file_put_contents("$receiverUrl/$subscriber/Messages/messages", "");
		file_put_contents("$receiverUrl/$subscriber/Messages/Notify", "false");
		file_put_contents("$receiverUrl/$subscriber/MemoryUtilityMetrics/memoryCapMonitor", "1048576");
		file_put_contents("$receiverUrl/$subscriber/Photos/recordedFileUploadNameSignatures",$basename);
	}

	// Create a Directory for yourself in each (existing) account's Friend folder
	foreach($Subscribers as $subscriber) {
		if($subscriber == $username) continue;
		if ($subscriber == ".DS_Store") continue;

		$subscriberDirPathForUser = "../Accounts/Members/$subscriber/Friends";
		mkdir("$subscriberDirPathForUser/$username");
		mkdir("$subscriberDirPathForUser/$username/Photos");
		mkdir("$subscriberDirPathForUser/$username/Messages");
		mkdir("$subscriberDirPathForUser/$username/MemoryUtilityMetrics");

		// Create the necessary file in each folder above
		file_put_contents("$subscriberDirPathForUser/$username/Messages/messages", "");
		file_put_contents("$subscriberDirPathForUser/$username/MemoryUtilityMetrics/memoryCapMonitor", "1048576");
		file_put_contents("$subscriberDirPathForUser/$username/Photos/recordedFileUploadNameSignatures", "");
		file_put_contents("$subscriberDirPathForUser/$username/Messages/Notify", "false");
	}

	// Other ..
	file_put_contents("$receiverUrlInitDirUrl/LastRecipient", $username);
	file_put_contents("../Accounts/Members/$username/Init/passcode", $passcode);
	file_put_contents("../Accounts/Members/$username/Init/Username", $username);
	file_put_contents("../Accounts/Members/$username/Init/ColorScheme", $colorScheme);
	file_put_contents("../Accounts/Members/$username/Init/Screenshot", $basename);
	move_uploaded_file($tempLocationForUploadedFile, "../Accounts/Members/$username/Init/$basename");

	echo "$username";
?>
