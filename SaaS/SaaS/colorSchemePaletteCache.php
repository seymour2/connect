<?php
	$cachedColor = $_POST["cachedColor"];
	$cachedColorType = $_POST["cachedColorType"];

	$LocationToRecordCachedColor = "../Init/ColorSchemePalette/";

	switch ($cachedColorType) {
		case "header":
			file_put_contents($LocationToRecordCachedColor."messageHeader", $cachedColor);
			break;

		case "searchBar":
			file_put_contents($LocationToRecordCachedColor."resourceSearchBar", $cachedColor);
			break;

		case "messageBox":
			file_put_contents($LocationToRecordCachedColor."cacheColorForTextBox", $cachedColor);
			break;	

		case "fPaneBgColor":
			file_put_contents($LocationToRecordCachedColor."friendsPaneColorScheme", $cachedColor);
		break;
		
		default:
			echo "failure to cache color: $cachedColor";
			break;
	}
	echo "Color cached: $cachedColor";
?>