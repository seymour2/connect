<?php
	$recipient = $_POST['recipient'];
	$TypeOfUploadedFile = $_FILES["attachment"]["type"];
	$username = file_get_contents("../Init/Username");
	$basename = basename($_FILES["attachment"]["name"]);
	$sizeOfUploadedFile = $_FILES["attachment"]["size"];
	$nameOfUploadedFile = $_FILES["attachment"]["name"];
	$initialsColorScheme = file_get_contents("../Init/ColorScheme");
	$tempLocationForUploadedFile = $_FILES["attachment"]["tmp_name"];
	$destinationToSendTheUploadedFile = "../../../SharedAttachmentSent/PictureFolder/";
	$PictureStorageFile = "../../SharedAttachmentSent/PictureFolder/";
	$totalFileUploadStorageForEachFriendUrl = "../Friends/$recipient/MemoryUtilityMetrics/memoryCapMonitor";
	$nameSignatureForFile = "$basename\n";

	$typeToParse; // audio, image or video element

	switch($TypeOfUploadedFile) {

		case "image/jpeg":
		$typeToParse = "img";
		break;

		case "image/jpg":
		$typeToParse = "img";
		break;

		case "audio/mpeg":
		$typeToParse = "audio";
		break;

		case "video/mp4":
		$typeToParse = "video";
		break;

		default: 
		echo "BadRequestException(400)";
		exit();
	}
	// Once the type of upload is configured customize the element to created <img> <audio> etc. 
	$pictureAttachmentFormat = "
		<div class=\"attachment\">
			<h3><b style=\"background-color: $initialsColorScheme\">$username[0]</b> $username</h3>
			<div class=\"frame\">
				<$typeToParse src=\"../../../SharedAttachmentSent/PictureFolder/$basename\" title=\"$basename\" controls>
				<h3>source: $username</h3>
			</div>
		</div>\n
	";

// -------- ------ --- --------- ------- ------ -------
	/*
			NOTE: Uploaded file size is measured in bytes !not kb 
			NOTE: 1MB is equal to (1,048,576) Bytes

			Megabytes (MB)	Kilobytes (KB) decimal	Kilobytes (KB) binary
				1 MB		1,000 KB							1,024 KB
				2 MB		2,000 KB							2,048 KB
				3 MB		3,000 KB							3,072 KB
				4 MB		4,000 KB							4,096 KB
	*/


	// Keep the status of whether a step should be repeated or not
	$uploadAlreadyExist = file_exists($destinationToSendTheUploadedFile . $basename);
	//echo $sizeOfUploadedFile;

	// Record the size of the attachment --if the file does not already exist
	// Storage url: SharedAttachmentSent/PictureFolder/storage
	// Rename
	if (!$uploadAlreadyExist) {
		$sizeOfTotalSharedPictures = file_get_contents($PictureStorageFile);
		$sizeOfTotalSharedPictures += $sizeOfUploadedFile;

		// Record to the overall storage for each picture
		file_put_contents($PictureStorageFile, $sizeOfTotalSharedPictures);

		// (user) keep track of the total file upload memory for --all sent attachments (for recp)
		$currentUploadMemoryUsage = file_get_contents($totalFileUploadStorageForEachFriendUrl);
		$currentUploadMemoryUsage = ($currentUploadMemoryUsage + $sizeOfUploadedFile);
		file_put_contents($totalFileUploadStorageForEachFriendUrl, $currentUploadMemoryUsage);
	}

	// Structure the attachment to parse to the recipient and user's inbox
	// Do not upload the file if file_put_contents throws any errors
	$userUrl = "../Friends/$recipient/Messages/messages";
	if (file_exists("../../$recipient/Friends/$username") != true) {
		mkdir("../../$recipient/Friends/$username");
	}
	if (file_exists("../../$recipient/Friends/$username/Messages") != true) {
		mkdir("../../$recipient/Friends/$username/Messages");
	}
	
	// Create ^
	$receiverUrl = "../../$recipient/Friends/$username/Messages/messages";
	$userPathExist = file_put_contents($userUrl, $pictureAttachmentFormat, FILE_APPEND | LOCK_EX);
	if ($recipient != $username) {
		// Prevents Duplication between content shared with --self
		file_put_contents($receiverUrl, $pictureAttachmentFormat, FILE_APPEND | LOCK_EX);
	}
	
	// Create the photos folder if it does not already exist
	// Set remaining Dependencies 
	if (file_exists("../Friends/$recipient/Photos") != true) {
		mkdir("../Friends/$recipient/Photos");
	}

	// Write the file to the disk after logistics are recorded -- if it does not aleady exist
	if (!$uploadAlreadyExist) {
		if (move_uploaded_file($tempLocationForUploadedFile, $destinationToSendTheUploadedFile . $basename)) {
			echo "file uploaded successfully";
		} else {
			echo "error, file was not uploaded";
		}
		//echo $tempLocationForUploadedFile;
		// Record the name of the file that was uploaded for later reference
		file_put_contents("../Friends/$recipient/Photos/recordedFileUploadNameSignatures", $nameSignatureForFile, FILE_APPEND);
	}

	// Update the last recipient for auto loading content on startUp page
	file_put_contents("../Friends/init/LastRecipient", $recipient);

	// Set Notify to --true to allow Live Notification update and import
	file_put_contents("../../$recipient/Friends/$username/Messages/Notify", "true");
	file_put_contents("../../$username/Friends/$recipient/Messages/Notify", "true");
?>
