  <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../../../Configuration/Stylesheets/sidePane/main.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
</head>
<body>
	<div class="sidePane">
		<?php
		$username = file_get_contents("../Init/Username");
		$Screenshot = file_get_contents("../Init/Screenshot");
		$defaultPhotos = scandir("../../../../Configuration/Images/Backgrounds");
		unset($defaultPhotos[0]); // .
		unset($defaultPhotos[1]); // ..

		// import the Gallery feature
			echo "<div class=\"Gallery\">";

			echo "<div class=\"Studio\">
					<div class=\"List-Friends\">
						<p>$username</p>
						<p></p>
						<p></p>
						<p></p>
					</div>
					<p>La gamme de bloqueurs de signaux disponibles sur le marché est large et ils peuvent être utilisés pour bloquer les signaux des téléphones portables à partir de différentes fréquences. La version portable peut être utilisée pour bloquer tous les signaux de téléphone portable et même les signaux GPS, WiFi et Lojack. Sa taille compacte et sa large plage de brouillage le rendent populaire parmi les utilisateurs.</p>

					<canvas id=\"CacheChart\"></canvas>
				</div>";

			echo  "<div class=\"sideBar\">";
				echo "<div class=\"headerBar\"> 
						<p>$username</p> 
					 </div>";
				
				// area --screenshot	
				echo "<div class=\"imageBar\">
						<img src=\"../Init/$Screenshot\" id=\"GalleryProfilePicture\">
						<form id=\"formToUpdateProfilePicture\">
							<input type=\"file\" name=\"profilePictureUpdate\" id=\"pictureUpdate\">
						</form>
					</div>";

				// Default Gallery Photos
				$stopImportingPhotos = 0;
				echo "<div class=\"PhotoSlideDeck\">";
						foreach($defaultPhotos as $BG_Photo) {
							if ($BG_Photo == ".DS_Store") continue;
							if ($BG_Photo == "Etsy.png") continue;
							if($stopImportingPhotos == 3) break;
							$stopImportingPhotos += 1;
							echo "<img src=\"../../../../Configuration/Images/Backgrounds/$BG_Photo\">";
						} 
				echo "</div>";

			echo "</div>";

			echo "<div class=\"BubbleSelector\"><div class=\"Cache\"></div></div>";
			echo "</div>";
		?>
		<!-- import Document feature -->
			<div class=Document>
				<p id="pdfHeading">@Screenshot</p>
				<div class=Studio>
					<embed src="../Documents/8 Drawer Dresser.pdf#toolbar=0" id="pdf">
				</div>

				<div class="sideBar">
					<div class=headerBar><h3>@Screenshot</h3></div>
					
					<div class="imageBar">
						<img src="../../../../Configuration/Images/Backgrounds/pexels-pixabay-163872.jpg">
						<form id="formToUploadPdf">
							<input type="file" name="pdf" id="pdfUpdate">
						</form>
					</div>

					<!-- Catalog of pdfs -->
					<div class="Catalog">
						<?php
							$pdfs = scandir("../Documents");
							unset($pdfs[0]); // .
							unset($pdfs[1]); // ..

							foreach($pdfs as $pdf) {
								if($pdf == ".DS_Store") continue;
								echo "<li>$pdf</li>";
							}
						?>
					</div>
					<div class="BubbleSelector" id="CLOSE_DOC"> <div class="Cache"></div> </div>
				</div>

			</div>
		<?php
		// Search bar for resources
		$cachedColor = file_get_contents("../Init/ColorSchemePalette/resourceSearchBar");
			echo "
				<div class=\"searchBar\" style=\"background-color: $cachedColor\">
					<img src=\"../../../../Configuration/Images/Resources/icons8-world-48.png\">
					<input type=\"search\" id=\"rSearch\" placeholder=\"rechercher un widget\">
				</div>";
		?>

		<!-- import ringtone feature -->
		<div class="Ringtones">
			<h3><b>Reset Default Tone</b> Select from the list of tones below to change your default notification sound. Once you click on a song it will change to your default tone. To revert the change you will have to reselect another tone</h3>
			<h2>@Ringtones</h2>
			<div class="RingtoneSelection">
				<li>Air Horn Sound.mp3</li>
				<li>Gadi Horn - Notification.mp3</li>
				<li>I Lost My Mind - Billie Eilish English Song.mp3</li>
				<li>iPhone - Message Sound.mp3</li>
				<li>Message Tune - Water Drop Sound.mp3</li>
				<li>Skype Message.mp3</li>
				<li>Short.mp3</li>
				<li>Skype Short.mp3</li>
			</div>
			<div class="BubbleSelector" id="CLOSE_ringtone"><div class="Cache"></div></div>
		</div>

		<!-- Popup resources container -->
		<div class="Resources">
			<div class="resource" id="GalleryDashBoard">
				<div class="border"><img src="../../../../Configuration/Images/Resources/icons8-photo-gallery-64.png" title="photo"></div>
			</div>
			<div class="resource" id="PDF_DOCUMENTS">
				<div class="border"><img src="../../../../Configuration/Images/Resources/icons8-documents-67.png" title="profile"></div>
			</div>
			<div class="resource" id="Ringtones">
				<div class="border"><img src="../../../../Configuration/Images/Resources/icons8-notification-bell-64.png" title="Ringtone"></div>
			</div>
			<div class="resource">
				<div class="border" id="microphone"><img src="../../../../Configuration/Images/Resources/icons8-microphone-48.png" title="Speech2Text"></div>
			</div>
			<div class="resource" id="weatherReport">
				<div class="border"><img src="../../../../Configuration/Images/Resources/icons8-avalanche-50.png"title="weather"></div>
			</div>
			<div class="resource" id="MusicVideos">
				<div class="border"><img src="../../../../Configuration/Images/Resources/icons8-audio-64 (1).png" title="recording"></div>
			</div>
		</div>

		<!-- Google: map init -->
	     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11183.169340393692!2d-122.68794991186046!3d45.51425885174954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54950a1036189519%3A0xf54b75d54eea9242!2sPortland%20Downtown%2C%20Portland%2C%20OR!5e0!3m2!1sen!2sus!4v1657130248436!5m2!1sen!2sus" class="map" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

		<!-- Information about the logged in --user/sender -->
		<div class="face" id="UserRequest">
			<?php
				$sPicture = file_get_contents("../Init/Screenshot");
				$sUsername = file_get_contents("../Init/Username");

				echo "<img src=\"../Init/$sPicture\" id=\"HomepageProfilePicture\">";
				echo "<p id=\"user\">$sUsername</p>";
			?>
			<h3 id="timer"></h3>
		</div>
	</div>

	<script src="../../../../Configuration/Saas/ScriptsJs/sidePane/timer.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/resources/rSearch.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/voiceNote/voicenote.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/Gallery/pictureSection.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/attachments/document.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/resources/ringtone.js"></script>
</body>
</html>