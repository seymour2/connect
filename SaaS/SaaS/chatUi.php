<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../../../Configuration/Stylesheets/messagePane/main.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
</head>
<body>
	<div class="messageUi">

		<!-- The music app -->
		<div class="music">
			<div>
				<video src="../../../../Configuration/Records/audio/Electro-Light & Itro - Paradox.mp4" id="musicVideo" controls>
				<?php
					$mDeliveryTone = file_get_contents("../Init/MessageDeliveryTone");
					echo "
						<audio src=\"$mDeliveryTone\" id=\"messageDeliveryTone\" controls>
					";
				?>
			</div>
			<div class="musicDetails">
				<h3 id="musicNameLink">Electro-Light & Itro <b>Paradox</b></h3>
				<p>Notre tout premier conte original : Une baleine et un oiseau tombent amoureux. Tout est parfait. Mais le monde ne s'arrête pas de tourner simplement parce qu'un oiseau</p>
				<div class="albumCoverContainer">
					<?php
						$songs = array("3:53","3:41","4:09","3:18","3:09");
						$cover = array(
							"pexels-shonny-yu-2947103.jpg",
							"pexels-mark-neal-5179570.jpg",
							"pexels-oleksandr-pidvalnyi-1006360.jpg",
							"pexels-quang-nguyen-vinh-3355788.jpg",
							"pexels-pixabay-163872.jpg"
						);
						$coverIndex = 0;
						foreach($songs as $song) {
							echo "<div class=\"albumCover\">	
									<img src=\"../../../../Configuration/Images/Backgrounds/$cover[$coverIndex]\">
									<p>$song</p>
								  </div>";
							$coverIndex++;
						}
					?>
				</div>
				<div class="BubbleSelector" id="OFFBubble"> <div class="Cache"></div> </div>
			</div>
		</div>
		<!-- fileStatisticsContainerForReceivers: Bio, memory usage etc.-->
		<div class="fileStatisticsContainerForReceiver">
			<h2 id="receiver"></h2>
			<p>Un entretien avec Manuel Castells autour des thèses de son livre L’Ere de l’information. On lira également une analyse de Iosu Perales sur</p>
			<div id="memoryDisplay">
				<input type="range" id="rRange" min="0" max="50" value="20" disabled>
				<p id="memoryUsagePercentage">18%</p>
			</div>
			<div class="img-storage">
				<?php
					$Screenshot = file_get_contents("../Init/Screenshot");
					echo "<img src=\"../Init/$Screenshot\">";
				?>
				<img src="" id="rReceiver">
			</div>
			<canvas id="myChart"></canvas>
		</div>

		<!-- receiver DashBoard -->
		<div class="receiverDashboard">
			<?php
					$lastRecipient = file_get_contents("../Friends/init/LastRecipient");
					$Screenshot = file_get_contents("../../$lastRecipient/Init/Screenshot");
					echo "<img src=\"../../$lastRecipient/Init/$Screenshot\" id=\"receiverDashImage\">";
			?>
			<div class="receiverDashboardDetails">
				<?php
					$username = file_get_contents("../Friends/init/LastRecipient");
					echo "<h2 id=\"receiverDashboardName\">$username</h2>";
				?>
				<p>La revue « binationale » de Ziad Abu-Zayyad et de Victor Cygelman analyse en profondeur la dimension</p>
				<div class="colorSelection">
					<p class="border"></p>
					<p class="border"></p>
					<p class="border"></p>
					<p class="border" id="numberOfFriendsLeft">+3</p>
				</div>
				<div class="images">
					<img src="../../../../Configuration/Images/Backgrounds/pexels-quang-nguyen-vinh-3355788.jpg">
					<img src="../../../../Configuration/Images/Backgrounds/pexels-quang-nguyen-vinh-3355788.jpg">
					<img src="../../../../Configuration/Images/Backgrounds/pexels-quang-nguyen-vinh-3355788.jpg">
				</div>
				<!-- The OFF btn to hide receiverDashboard -->
				<div class="BubbleSelector" id="BubbleSelectorOFF"> <div class="Cache"></div> </div>
			</div>
		</div>

		<!-- weather Report popup -->
		<div class="weatherReportStorage">
			<h3>Weather Report</h3>
			<p>Click anywhere outside weather to return</p>
			<script src="https://apps.elfsight.com/p/platform.js" defer></script>
            <div class="elfsight-app-96fe37d0-f340-4439-ab17-db45c43f99a4" id="weather"></div>
		</div>

		<!-- headerUi UX: rPicture, name, range-->
		<?php 
			$cachedColor = file_get_contents("../Init/ColorSchemePalette/messageHeader");
			echo "<div class=\"headerUi\" style=\"background-color: $cachedColor\">"; 
		?>
			<!-- recipient init -->
			<div class="subscriberMessageHeader">
				<?php
					$lastRecipient = file_get_contents("../Friends/init/LastRecipient");
					$recipientPicture = file_get_contents("../../$lastRecipient/Init/Screenshot");
					echo "<img id=\"rPicture\" src=\"../../$lastRecipient/Init/$recipientPicture\">";
					echo "<p id=\"recipient\">$lastRecipient</p>";
					echo "<input type=\"color\" id=\"bgColor\" value=\"#f6b73c\">";
				?>
			</div>
  			<input type="range" id="vol" min="0" max="50" value="25">
			<!-- tools init -->
		<?php echo "</div>";?>

		<!-- messagesSentUi: This is where sent and received messages are seen-->
		<div class="contentUi" id="sharedMsg">
			<?php
			// Get the messages for the last recipient (sent to)
			$lastRecipientUrl = "../Friends/init/LastRecipient";
			$lastRecipient = file_get_contents($lastRecipientUrl);
			$lastRecipientMessages = file_get_contents("../Friends/$lastRecipient/Messages/messages");

			echo $lastRecipientMessages;
			?>
		</div>
		
		<!-- sendBoxUi: Display the name of the user in the message box by default-->
		<?php 
			$cachedColor = file_get_contents("../Init/ColorSchemePalette/cacheColorForTextBox");
			echo "<div class=\"sendUi\" style=\"background-color: $cachedColor\">"; 
		?>
			<?php
			    date_default_timezone_set('America/Los_Angeles');
				$user = file_get_contents("../Init/Username");
				$hour = date("H"); $Greeting = "Bonjour";
				$ColorScheme = file_get_contents("../Init/ColorScheme");

				if ($hour > 13 && $hour < 19) $Greeting = "Bonsoir";
				else if ($hour > 19 && $hour < 23) { $Greeting = "Bonne nuit";}

				echo "<p class=\"border\" style=\"background-color: $ColorScheme\" id=\"receiverDashboard\">$user[0]</p>";
				echo "<input type=\"text\" id=\"messagecnt\" name=\"message\" placeholder=\"$Greeting $user\" maxlength=\"830\" autocomplete=\"off\">";
			?>
			<p id="wordCount">+0</p>
			<!-- Attachment processing -->
			<form id="form-id" enctype="multipart/form-data" style="display: none;">
	            <input type="file" name="attachment" id="file">
	            <input type="text" name="recipient" id="pRecipient">
	        </form>
			<label for="file" id="sender"><img src="../../../../Configuration/Images/Resources/icons8-attachment-48.png"></label>
		<?php echo "</div>"; ?>

		<!-- <div class="Editor"> -->
			<div class="EditorTools">
				<div class="FontSelection"><h3>OGuaue <img src="../../../../Configuration/Images/Resources/icons8-chevron-24.png" width="10" height="10"></h3></div>
				<div class="TextDecoration">
					<img src="../../../../Configuration/Images/EditorTools/icons8-bold-64.png">
					<img src="../../../../Configuration/Images/EditorTools/icons8-italic-30.png">
					<img src="../../../../Configuration/Images/EditorTools/icons8-underline-32.png">
				</div>
				<div class="FontColorSelection">
					<input type="color" value="#000000">
					<input type="color" value="#7FFF00">
					<input type="color" value="#006400">
				</div>
				<div class="Alignment">
					<img src="../../../../Configuration/Images/EditorTools/icons8-left-align-64.png">
					<img src="../../../../Configuration/Images/EditorTools/icons8-center-align-64.png">
					<img src="../../../../Configuration/Images/EditorTools/icons8-right-align-64.png">
				</div>
				<div class="ListSelection">
					<img src="../../../../Configuration/Images/EditorTools/icons8-bulleted-list-32.png">
					<img src="../../../../Configuration/Images/EditorTools/icons8-numbered-list-48.png">
				</div>
			</div>
		<!-- </div> -->
	</div>
	<script src="../../../../Configuration/Saas/ScriptsJs/chatUi/header.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/chatUi/cSearch.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/chatUi/messageDelivery.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/attachments/attachment.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/chatUi/receiverDashboard.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/music/music.js"></script>
</body>
</html>