<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../../../Configuration/Stylesheets/friendsPane/main.css">
	<title>friendsUi</title>
</head>
<body onload="GetColorScheme()">
	<div class="friendsUi">
		<!-- The search element: for friends -->
		<div class="searchBar">
			<div class="border"><img src="../../../../Configuration/Images/Resources/icons8-chart-67.png"></div>
			<input type="search" id="cSearch" onkeyup="cSearch()" placeholder="chercher un ami">
		</div>

		<!-- The (element container) for user's: friends -->
		<div class="friends" id="contacts">
			<?php
				$FriendsDirectoryPath = "../Friends";
				$ListOfFriends = scandir($FriendsDirectoryPath);
				unset($ListOfFriends[0]); // .
				unset($ListOfFriends[1]); // ..

				$contactsInMemberDir = scandir("../../"); // Members .. 
				unset($contactsInMemberDir[0]); // .
				unset($contactsInMemberDir[1]); // ..

				if ($ListOfFriends) {			
					// Import contacts that are not in ../Friends url
					if ($contactsInMemberDir) {
						foreach ($contactsInMemberDir as $friend) {
							if ($friend == ".DS_Store") continue;
							
							$contactInMemberDir = $friend;
							foreach($ListOfFriends as $friend) {
								if (file_exists("$FriendsDirectoryPath/$contactInMemberDir") != true) {
									// Create folder for friend
								mkdir("$FriendsDirectoryPath/$contactInMemberDir");
								mkdir("$FriendsDirectoryPath/$contactInMemberDir/Photos");
								mkdir("$FriendsDirectoryPath/$contactInMemberDir/Messages");
								mkdir("$FriendsDirectoryPath/$contactInMemberDir/MemoryUtilityMetrics");
								// Create ^^
								
								file_put_contents("../Friends/$friend/Photos/recordedFileUploadNameSignatures", "");
								file_put_contents("$FriendsDirectoryPath/$contactInMemberDir/Messages/Notify","false");
								file_put_contents("$FriendsDirectoryPath/$contactInMemberDir/Messages/messages", "");
								file_put_contents("$FriendsDirectoryPath/$contactInMemberDir/MemoryUtilityMetrics/memoryCapMonitor", "1048576");
								}
							}
						}
					}

					foreach ($ListOfFriends as $friend) {
						if ($friend == "init") continue;
						if ($friend == ".DS_Store") continue;

						if(file_exists("../Friends/$friend/MemoryUtilityMetrics/memoryCapMonitor") != true) {
							// Memory Sent
							mkdir("../Friends/$friend/MemoryUtilityMetrics");
							file_put_contents("../Friends/$friend/MemoryUtilityMetrics/memoryCapMonitor", "1048576");

							// Photos
							mkdir("../Friends/$friend/Photos");
							file_put_contents("../Friends/$friend/Photos/recordedFileUploadNameSignatures", "");
						}
						// Get the total memory usage for each friend (recp)
						$memory = file_get_contents("../Friends/$friend/MemoryUtilityMetrics/memoryCapMonitor");
						$memory = ($memory/1048576);
						$ColorScheme = file_get_contents("../../$friend/Init/ColorScheme");

						echo "
							<div class=\"sThread\">
								<p class=\"border\" style=\"background-color: $ColorScheme\">$friend[0]</p>
								<h3>$friend</h3>
								<input type=\"range\" class =\"rMsgContent\" min=\"0\" max=\"50\" value=\"$memory\" disabled>
							</div>";
					}
				}
			?>
		</div>

		<!-- The (element container) for the Group messaging -->
		<div class="Groups">
			<h2>Groups</h2>
			<?php
				$GroupPath = "../../../../SaaS/Groups";
				$Groups = scandir($GroupPath);
				unset($Groups[0]); // .
				unset($Groups[1]); // ..
				unset($Groups[2]); // .DS_Store

				if ($Groups) {
					foreach ($Groups as $Group) {
						$bgColor = file_get_contents("$GroupPath/$Group/bgColor");
						$storage = file_get_contents("$GroupPath/$Group/storage");
						$img = file_get_contents("../../../../SaaS/Groups/$Group/img");
						//Query the Groups Dir
						echo "
							<div class=\"Group\">
								<div class=\"sThread\">
									<p class=\"border\" style=\"background-color: $bgColor\"><img src=\"../../../../Configuration/Images/Backgrounds/$img\"></p>
									<h3>$Group</h3>
									<input type=\"range\" class=\"rMsgContent\" min=\"0\" max=\"50\" value=\"$storage\">
								</div>
							</div>
						";
					}
				}
			?>
		</div>

		<!-- Color Schemes for changing the font, bgColor & search bgColor -->
		<div class="colorSchemes">
			<input type="color" id="fPaneBgColor" value="#A59B88">
			<input type="color" id="searchBarBgColor" value="#F6B73C">
			<input type="color" id="messageBox" value="#A6AAC9">
			<input type="color" id="messageBoxFontColor" value="#C40E0E">
			<input type="color" value="#CE3BF7">
		</div>
	</div>
	<script src="../../../../Configuration/Saas/ScriptsJs/chatUi/cSearch.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/chatUi/swapContact.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/chatUi/weatherReport.js"></script>
	<script src="../../../../Configuration/Saas/ScriptsJs/colorScheme/colorPalette.js"></script>
</body>
</html>