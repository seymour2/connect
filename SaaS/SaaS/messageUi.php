<?php
	$user = $_POST["sender"];
	$message = $_POST["message"];
	$recipient = $_POST["recipient"];


	$userUrl = "../Friends/$recipient/Messages/messages";
	$recipientUrl = "../../$recipient/Friends/$user/Messages/messages";
	$ColorScheme = file_get_contents("../Init/ColorScheme");

	// Structure the user | recipient message
	$userMessage = 
	"
	<div class=\"messageFrame\">
		<p class=\"border\" style=\"background-color: $ColorScheme\">$user[0]</p>
		<div class=\"messageContent\">
			<h3>$user</h3>
			<p>$message</p>
		</div>
	</div>
	\n";

	$recipientMessage = 
	"
	<div class=\"messageFrame\">
		<p class=\"border\" style=\"background-color: $ColorScheme\">$user[0]</p>
		<div class=\"messageContent\">
			<h3>$user</h3>
			<p>$message</p>
		</div>
	</div>
	\n";

	// Write the message to both the recipient | user's inbox
	$mPathPointer = file_put_contents($userUrl, $userMessage, FILE_APPEND | LOCK_EX);
	if ($recipient != $user) {
	    $mPathPointer = file_put_contents($recipientUrl, $recipientMessage, FILE_APPEND | LOCK_EX);
	}
	$mPathPointer = file_put_contents("../Friends/init/LastRecipient", $recipient, LOCK_EX);

	// Set Notify to --true to allow Live Notification update and import
	file_put_contents("../../$recipient/Friends/$user/Messages/Notify", "true");
	file_put_contents("../../$user/Friends/$recipient/Messages/Notify", "true");
?>