<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../../../Configuration/Stylesheets/userPage/main.css">
	
	<?php
		$username = file_get_contents("../Init/Username");
		echo "<title>$username</title>";
	?>

	<style>
		body { 
		width: 100%; 
		border: none;
		height: 100vh;  
		overflow: hidden;
		background-size: cover;
		background-image: url("../../../../Configuration/Images/Backgrounds/Screenshot 2022-10-04 at 1.20.43 PM.png"); }
	</style>
</head>
<body>
	<?php
		include("profile.php");
		include("chatUi.php");
		include("friendsUi.php");
	?>
</body>
</html>