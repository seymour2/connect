<?php
	$ringtone = $_POST["ringtone"];

	$pathToRingtoneRequested = "../../../../SaaS/Ringtones/$ringtone";
	$destToStoreRequestedRingtone = getcwd() ."/$ringtone";
	$defaultRingtoneToReset = file_get_contents("../Init/MessageDeliveryTone");

	if (copy($pathToRingtoneRequested, $destToStoreRequestedRingtone)) {
		unlink($defaultRingtoneToReset);
		file_put_contents("../Init/MessageDeliveryTone",$ringtone);
		echo("true");
	}
?>