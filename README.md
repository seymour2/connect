# connect 
![screenshot - Home](screenshot3.png)
![screenshot - Home](screenshot1.png)
![screenshot - Home](screenshot2.png)
![screenshot - Home](screenshot4.png)
![screenshot - Home](screenshot.png)
![ringtone - weather](ringtone_weather.png)
![memory usage per friend](memory.png)
![file storage](files.png)

# Dependencies 
In order to run the site you need a [XAMPP Server](https://www.apachefriends.org/) installed. Once the ```server``` is installed you need to move the folder ```connect``` into the ```htdocs``` folder. You can find the ```htdocs``` folder inside the ```Applications/XAMPP/xamppfiles/htdocs```. If you are using windows you need to navigate to the location where the xampp files were stored on installation. 

Once everything is setup you need to run the ```XAMPP Application``` manager-os (if on mac). Navigate to ```Manage Servers``` and start the ```Apache web server```. Next, open a web browser and type ```localhost/connect```. This will bring up the landing page. 

# Features
- Share messages
- Share pictures
- Share audio
- share video
- upload pdf documents
- watch a selected amount of video
- check the weather
- change the color scheme for the ```friends/group``` pane etc
- change default notification tone
- change default profile picture
- Voice record messages you want to share
