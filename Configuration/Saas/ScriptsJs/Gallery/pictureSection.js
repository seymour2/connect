document.querySelector("#pictureUpdate").addEventListener("change", function(event){
    // add previous photo to gallery first
    let previousProfilePhoto = document.createElement("img");
    previousProfilePhoto.src = document.querySelector("#HomepageProfilePicture").src;
    document.querySelector(".PhotoSlideDeck").prepend(previousProfilePhoto);
    
	// Parse
	var eventSrc = URL.createObjectURL(event.target.files[0]);
	document.querySelector("#GalleryProfilePicture").src = eventSrc;
	document.querySelector("#HomepageProfilePicture").src = eventSrc;
	var user = document.querySelector("#user").innerHTML;
	var recipient = document.querySelector("#recipient").innerHTML;
	if(user === recipient) {
	    document.querySelector("#rPicture").src = eventSrc;
	    document.querySelector("#receiverDashImage").src = eventSrc;
	}

	// Send to server 
	var request = new XMLHttpRequest();
	request.open("POST", "updateProfilePicture.php");

	request.onload = function() {
		if(this.readyState == 4 && this.status == 200) {
			// Later ..
		} else if (this.readyState == 4 && this.status == 404) {
			alert("NOT FOUND");
		} else {
			// Later ..
		}
	}
	request.send(new FormData(document.querySelector("#formToUpdateProfilePicture")));
}, false);

// Update (default) screenshot
document.querySelector(".PhotoSlideDeck").addEventListener("mouseover",function(){
	var defaultBackgroundScreenshots = this.children;
	var user = document.querySelector("#user").innerHTML;
	var recipient = document.querySelector("#recipient").innerHTML;

	for (var i = 0; i < defaultBackgroundScreenshots.length; ++i) {
		this.children[i].onclick = function() {
			document.querySelector("#GalleryProfilePicture").src = this.src;
			document.querySelector("#HomepageProfilePicture").src = this.src;
		}
	}
},false);