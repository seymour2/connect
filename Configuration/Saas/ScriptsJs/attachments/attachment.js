// Dasboard image index for new uploads (top three) added
var DashboardImageIndex = 0;
var username = document.querySelector("#user").innerHTML;

function ParseAttachmentForPicture(event) {
	var contacts = document.querySelector("#contacts").children;
	var recipient = document.querySelector("#recipient").innerHTML;

	// Create the elements for the picture frame --attachment
	var user = document.createElement("h3");
	var reaction = document.createElement("h3");
	var attachment = document.createElement("div");
	var attachmentFrame = document.createElement("div");
	var fileUploadType;

	// Find the rececipient and update the memory usage
	for (var i = 0; i < contacts.length; ++i) {
		var receiver = contacts[i].children;
		if (receiver[1].innerHTML == recipient) {
			var currentStorage = receiver[2].value;
			var uploadFileSize = Math.ceil(event.target.files[0].size/1048576);
			var updatedCacheSize = (parseInt(currentStorage) + parseInt(uploadFileSize));
			receiver[2].value =  updatedCacheSize;
			break;
		}
	}
	// Use switch to create the designated upload type
	switch(event.target.files[0].type) {
		case "audio/mpeg":
		fileUploadType = document.createElement("audio");
		if (fileUploadType.canPlayType("audio/mpeg")) {
			fileUploadType.setAttribute("src", URL.createObjectURL(event.target.files[0]));
			fileUploadType.setAttribute("controls","controls");
			reaction.style.display = "none";
		}
		break;

		case "image/jpeg":
		//reaction.innerHTML = "SaaS";
		reaction.innerHTML = `source: ${username}`;
		fileUploadType = document.createElement("img");
		fileUploadType.title = "SaaS";
		fileUploadType.src = URL.createObjectURL(event.target.files[0]);
		// Keep a copy of the image in the receiver's gallery view
		let case1 = document.createElement("img");
		case1.title = event.target.files[0].name;
		case1.src = URL.createObjectURL(event.target.files[0]);
		document.querySelector(".PhotoSlideDeck").prepend(case1);
		break;

		case "image/jpg":
		reaction.innerHTML = `source: ${username}`;
		// Keep a copy of the image in the receiver's gallery view
		let galleryImageToAdd = document.createElement("img");
		galleryImageToAdd.title = event.target.files[0].name;
		galleryImageToAdd.src = URL.createObjectURL(event.target.files[0]);
		document.querySelector(".PhotoSlideDeck").prepend(galleryImageToAdd);
		break;

		case "video/mp4":
		fileUploadType = document.createElement("video");
		fileUploadType.setAttribute("src", URL.createObjectURL(event.target.files[0]));
		fileUploadType.setAttribute("controls","controls");
		reaction.style.display = "none";
		break;

		default: 
		//alert(`${event.target.files[0].type} is not supported at the moment.`);
		return false;
	}

	// Send a request to the the background color for the initials Bubble
	var senderInitialsBG;
	const request = new XMLHttpRequest();
	request.open("POST", "../Init/ColorScheme");
	request.onload = function() {
		if(this.readyState == 4 && this.status == 200) {
			senderInitialsBG = this.response;
			// Set the content for each element that is --created
			user.innerHTML = `<b style="background-color: ${senderInitialsBG}">${username[0]}</b> ${username}`;
		} else {
			// Set the content for each element that is --created
			user.innerHTML = `<b style="background-color: pink">${username[0]}</b> ${username}`;
		}
	}
	request.send();

	// Set the --class attribute for each element --required
	attachment.setAttribute("class", "frame");
	attachmentFrame.setAttribute("class", "attachment");

	// append attributes to class object containers --elements
	attachment.append(fileUploadType);
	attachment.append(reaction);
	attachmentFrame.append(user);
	attachmentFrame.append(attachment);

	document.querySelector(".contentUi").append(attachmentFrame);
	// Play delivery tone --Short.mp3
	document.querySelector("#messageDeliveryTone").play();
	return true;
}

function DeliverPictureSent() {
	if(ParseAttachmentForPicture(event) != true) return;

	// Sroll to focus on last message sent
	var focusLastMessageSent = document.querySelector(".contentUi").children;
	focusLastMessageSent[focusLastMessageSent.length - 1].scrollIntoView();

	// Set the recipient of the picture before the form is constructed
	var recipient = document.querySelector("#recipient").innerHTML;
	document.querySelector("#pRecipient").value = recipient;

	// Construct the form to send after recipient is set
	var pictureForm = document.querySelector("#form-id");
	var pictureFormData = new FormData(pictureForm);

	const request = new XMLHttpRequest();
	request.open("POST", "fileUpload.php");

	// Get the status of the uploaded attachment for LOG
	request.onload = function () {
		if(this.readyState == 4 && this.status == 200) {
			 document.querySelector("#form-id").reset();
			 LogErrorMessages(recipient, this.response);
		} else {
			// File does not exist etc.
			LogErrorMessages(recipient, this.response);
		}
	}
	request.send(pictureFormData);
}

// LOG ERROR Messages
function LogErrorMessages(receiver, errorMessage) {
			var receiver = receiver;
			var ISOTimeOBJ = new Date();
			var ERROR = errorMessage;

			// Set the input
			var userInput = document.createElement("input");
			userInput.setAttribute("type", "text");
			userInput.setAttribute("name", "ISOTimeOBJ");
			userInput.setAttribute("value", `${ISOTimeOBJ.getMonth()}-${ISOTimeOBJ.getDay()}-${ISOTimeOBJ.getFullYear()}`);

			var receiverInput = document.createElement("input");
			receiverInput.setAttribute("type", "text");
			receiverInput.setAttribute("name", "receiver");
			receiverInput.setAttribute("value", receiver);

			var error_message_input = document.createElement("input");
			error_message_input.setAttribute("type", "text");
			error_message_input.setAttribute("name", "ERROR");
			error_message_input.setAttribute("value", ERROR);

			// Create the form
			var ERROR_FORM = document.createElement("form");
			ERROR_FORM.append(userInput);
			ERROR_FORM.append(receiverInput);
			ERROR_FORM.append(error_message_input);

			var formData = new FormData(ERROR_FORM);

			// Send the ERROR message request to ERROR_LOG.php --ajax
			const request = new XMLHttpRequest();
			request.open("POST", "ERROR_LOG.php");

			request.onload = function() {
				if (this.readyState == 4 && this.status == 200) {
					// Later ..
				} 
			}
			request.send(formData);
}

document.querySelector("#file").addEventListener("change", DeliverPictureSent);
