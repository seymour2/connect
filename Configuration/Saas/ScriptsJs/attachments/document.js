// Select PDF Document to parse
document.querySelector(".Catalog").addEventListener("mouseover", function(){
	var Catalog = this.children;

	for(var i = 0; i < Catalog.length; ++i) {
		Catalog[i].onclick = function() {
			document.querySelector("#pdfHeading").innerHTML = `@Screenshot ${this.innerHTML}`;
			document.querySelector("#pdf").src = `../Documents/${this.innerHTML}#toolbar=0`;
		}
	}
}, false);

// Upload PDF Documents
document.querySelector("#formToUploadPdf").addEventListener("change", function(event){
	if(event.target.files[0].type != "application/pdf") {
		alert(`${event.target.files[0].type} is not supported`);
		document.querySelector("#formToUploadPdf").reset();
		return;
	}

	// Send request to store pdf on server
	var request = new XMLHttpRequest();
	request.open("POST", "uploadPdfDocument.php");

	request.onload = function() {
		if(this.readyState == 4 && this.status == 200) {
			// Later ..
			if(this.response == false) {
				alert(`(Duplication) You already have a copy of ${event.target.files[0].name}`);
				document.querySelector("#formToUploadPdf").reset();
				return;
			}
			let uploadedPdf = document.createElement("li");
			uploadedPdf.innerHTML = event.target.files[0].name;
			document.querySelector(".Catalog").append(uploadedPdf);
			document.querySelector("#formToUploadPdf").reset();
		} else if (this.readyState == 4 && this.status == 404) {
			alert("NOT FOUND");
		} else {
			// Later ..
		}
	}
	request.send(new FormData(document.querySelector("#formToUploadPdf")));
}, false);

// Open PDF Documents 
document.querySelector("#PDF_DOCUMENTS").addEventListener("click", function(){
	document.querySelector(".Document").style.display = "flex";
}, false);

// close PDF Documents 
document.querySelector("#CLOSE_DOC").addEventListener("click", function(){
	document.querySelector(".Document").style.display = "none";
}, false);