function searchResources() {
	var resources = document.querySelector(".Resources").children;
	var search = document.querySelector("#rSearch").value.toUpperCase();

	for (var i = 0; i < resources.length; i++) {
		var resource = resources[i].children;
		var imageContaner = resource[0].children;
		var image = imageContaner[0];

		// Search
		if (image.title.toUpperCase().indexOf(search) > -1) {
			resources[i].style.display = "flex";
		} else {
			resources[i].style.display = "none";
		}
	}
}

document.querySelector("#GalleryDashBoard").addEventListener("click", function(){
	document.querySelector(".Gallery").style.display = "flex";
	// Pinn top three friends
	var friends = document.querySelector(".friends").children;
	var pinnedContactsFromGallery = document.querySelector(".List-Friends").children;
	var numberOfTimesToLoop;
	if(friends.length <= 3 && friends.length > 0) {
		numberOfTimesToLoop = friends.length;
	} else {
		numberOfTimesToLoop = 4;
	}

	for(var i = 0; i < numberOfTimesToLoop; ++i) {
		pinnedContactsFromGallery[i].innerHTML = friends[i].children[1].innerHTML;
	}
	var dropdownArrow = document.createElement("img");
	dropdownArrow.src = "../../../../Configuration/Images/Resources/icons8-chevron-24.png";
	pinnedContactsFromGallery[0].append(dropdownArrow);

}, false);
document.querySelector(".BubbleSelector").addEventListener("click", function(){
	document.querySelector(".Gallery").style.display = "none";
}, false);
document.querySelector("#rSearch").addEventListener("keyup", searchResources, false);