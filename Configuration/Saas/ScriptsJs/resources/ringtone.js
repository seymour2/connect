// Update the default ringtone
document.querySelector(".RingtoneSelection").addEventListener("mouseover", function(){
	var Ringtones = this.children;

	for(var i = 0; i < Ringtones.length; ++i) {
		Ringtones[i].onclick = function() {
			SetDefaultRingtone(this.innerHTML);
		}
	}
}, false);


function SetDefaultRingtone(ringtone) {
	// Request
	const httpRequestToUpdateRingtone = new XMLHttpRequest();
	httpRequestToUpdateRingtone.open("POST", "UpdateDefaultRingtone.php");
	httpRequestToUpdateRingtone.onload = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (this.response == "true") {
				document.querySelector("#messageDeliveryTone").src = ringtone;
				document.querySelector("#messageDeliveryTone").play();
			}
		} else if (this.readyState == 4 && this.status == 404) {
			// Later ..
		} else { 
			// Later ..
		}
	}
	httpRequestToUpdateRingtone.send(new FormData(CreateFormToResetDefaultRingtone(ringtone)));
}

function CreateFormToResetDefaultRingtone(ringtone) {
	var formToUpdateRingtone = document.createElement("form");
	var inputFieldForRingtoneName = document.createElement("input");
	// input 
	inputFieldForRingtoneName.setAttribute("type", "text");
	inputFieldForRingtoneName.setAttribute("name", "ringtone");
	inputFieldForRingtoneName.setAttribute("value", ringtone);

	// Setup 
	formToUpdateRingtone.append(inputFieldForRingtoneName);
	return formToUpdateRingtone;
}
// CLOSE the ringtone selection
document.querySelector("#CLOSE_ringtone").addEventListener("click", function(){
	document.querySelector(".Ringtones").style.display = "none";
}, false);

// OPEN the ringtone selection
document.querySelector("#Ringtones").addEventListener("click", function(){
	document.querySelector(".Ringtones").style.display = "flex";
}, false);