// Get the message content for each recipient, and parse the message to the contentUi
function requestRec() {
	var fileStatisticsContainerForReceiver = document.querySelector(".fileStatisticsContainerForReceiver");
	var contacts = document.querySelector("#contacts").children;
	var sender = document.querySelector("#user").innerHTML;
	var errorMessge = "There has been an Internal error. Try again later";

	for (var i = 0; i < contacts.length; i++) {
		// Get the message content for reciver and update the new receiver
		contacts[i].onclick = function() {
			// If the unread status is set -- remove it
			if (this.children.length === 4) {
			    let unreadMessageStatus = this.children[3];
				unreadMessageStatus.style.display = "none";
			}

			let receiver = this.children;
			//receiver[1] --receiver
			document.querySelector("#recipient").innerHTML = receiver[1].innerHTML;
			document.querySelector("#receiverDashboard").innerHTML = receiver[0].innerHTML;
			document.querySelector("#receiverDashboard").style.backgroundColor = receiver[0].style.backgroundColor;

			// Update receiver's DashBoard
			document.querySelector("#receiverDashboardName").innerHTML = receiver[1].innerHTML;

			// Request for the messages (-> recipient)
			const request = new XMLHttpRequest();
		    request.open("POST", "QMessage.php", true);

		    request.onload = function() {
		    	if (this.readyState == 4 && this.status == 200) {
		    		// Get the message history from server and parse it
		    		document.querySelector("#sharedMsg").innerHTML = this.response;

		    		var scrollPageDown = document.querySelector(".contentUi").children;
		    		if (scrollPageDown && scrollPageDown.length > 0) {
		    			scrollPageDown[scrollPageDown.length - 1].scrollIntoView();
		    		}
		    	} else { alert(errorMessge) }

		    	// Create a request for the (prof) picture update
		    	const request = new XMLHttpRequest();
		    	request.open("POST", `../../${receiver[1].innerHTML}/Init/Screenshot`, true);

		    	request.onload = function() {
		    		if (this.readyState == 4 && this.status == 200) {
		    			document.querySelector("#rPicture").src = `../../${receiver[1].innerHTML}/Init/${this.response}`;
		    			//alert(`../../${receiver[1].innerHTML}/Init/${this.response}`);
		    			document.querySelector("#receiverDashImage").src = `../../${receiver[1].innerHTML}/Init/${this.response}`;
		    			//document.querySelector("#receiverDashboardGender").style.backgroundColor = receiver[0].style.backgroundColor;
		    		} else { alert(errorMessge) }
		    	}
		    	request.send();
		    }

		    // Create form to store the name of the recipient (-> messages) to Request
			var recToRequest = document.createElement("form");
			var rRecipient = document.createElement("input");
			rRecipient.setAttribute("type", "text");
			rRecipient.setAttribute("name", "recipient");
			rRecipient.setAttribute("value", receiver[1].innerHTML);

			recToRequest.append(rRecipient);

			var formDataForRecipient = new FormData(recToRequest);
		    request.send(formDataForRecipient);
		}
	}
}

// Get statistics for the files shared between recipients
// src: https://www.w3schools.com/js/tryit.asp?filename=tryai_chartjs_bars_colors
function GetStatistics(sender, receiver) {
	document.querySelector("#receiver").innerHTML = receiver.innerHTML;

	var xValues = ["January", "Februrary", "March", "April", "May"];
	var yValues = [20, 25, 30, 25, 6];
	var barColors = [
	  "#b91d47",
	  "#00aba9",
	  "#2b5797",
	  "#e8c3b9",
	  "#1e7145"
	];

	new Chart("myChart", {
	  type: "bar",
	  data: {
	    labels: xValues,
	    datasets: [{
	      label: receiver.innerHTML,
	      borderColor: "crimson",
	      backgroundColor: barColors,
	      data: yValues
	    }]
	  },
	  options: {
	    title: {
	      display: true,
	      text: "Memory Usage",
	      fontFamily: "JetBrains Mono"
	    }
	  }
	});

	// Request and upload the receiver's photo
	const request = new XMLHttpRequest();
	request.open("POST", `../../${receiver.innerHTML}/Init/Screenshot`);
	request.onload = function() {
		// Request the profile picture --ajax request
		var rReceiverPictureUrlPath = `../../${receiver.innerHTML}/Init/${this.response}`;
		//alert(rReceiverPictureUrlPath);
		document.querySelector("#rReceiver").src = rReceiverPictureUrlPath;
	}
	request.send();
}

// Live Nofifications. 
//src: https://stackoverflow.com/questions/7188145/call-a-javascript-function-every-5-seconds-continuously
setInterval(EnableAlertsForUnreadMessages, 5000);

function EnableAlertsForUnreadMessages() {
	const contacts = GetRecipients();

   contacts.forEach(function(receiver){
   	RecordUnreadMessages(receiver.children[1].innerHTML);
   });
}
// Check for (unread) incoming messages by seeing if Notify is {true}
function RecordUnreadMessages(receiver) {
	const user = document.querySelector("#user").innerHTML;

	const request = new XMLHttpRequest();
	request.open("POST", `../../${receiver}/Friends/${user}/Messages/Notify`, false);

   request.onload = function() {
   	switch(this.response) {
   		case "true":
	   		CreateUnreadStatus(receiver);
	   		GetRecipientUnreadMessages(receiver, user);
   		break;
   	}
   }
	request.send();
}

function GetRecipientUnreadMessages(receiver, user) {
	const contentUi = document.querySelector(".contentUi");
	const currentlySelectedRecipient = document.querySelector("#recipient").innerHTML;
	
	const request = new XMLHttpRequest();
	request.open("POST", `../../${receiver}/Friends/${user}/Messages/messages`);
	request.onload = function() {
		document.querySelector("#messageDeliveryTone").play();
		// Reset the Notify file to false to silence notifications
		// this is done so that we only get a notification if there
		// is a new incoming message from a recipient
		ResetNotify(receiver);

		// Append to contentUi only for currently selected contact
		if(currentlySelectedRecipient == receiver) {
			contentUi.innerHTML = this.response;
			var scroll = document.querySelector(".contentUi").children;
			if(scroll.length != 0) {
				scroll[scroll.length - 1].scrollIntoView();
			}
		}
	}
	request.send();
}

// creates unreada status for recipient
function CreateUnreadStatus(recipientToCreateStatusFor) {
	let recipients = GetRecipients();
	
	recipients.forEach(function(recipient,index){
		let currentRecipient = recipient.children;
		if (currentRecipient[1].innerHTML === recipientToCreateStatusFor) {
			if (currentRecipient.length === 4) {
		   		var unreadStatus = currentRecipient[3];
		   		var updatedMessageCount = unreadStatus.innerHTML.replace("+"," ");
		   		updatedMessageCount.trim();
		   		unreadStatus.innerHTML = `+${parseInt(unreadStatus.innerHTML) + 1}`;
		   		unreadStatus.style.display = "flex";
		   	} else {
		   		unreadStatus = document.createElement("p");
		   		unreadStatus.innerHTML = `+1`;
		   		unreadStatus.setAttribute("class", "unreadStatus");
		   		recipient.append(unreadStatus);
		   	}
		   	// if the recipient is out of view on the friends pane
		   	// when you recieve an unread message from them, scroll
		   	// so their unread status can be visible to user
		   	recipients[index].scrollIntoView();
		}
	});
}

function GetRecipients() {
	return Array.from(document.querySelector(".friends").children);
}
// Reset -- Disable notification for same person, if nothing new is: incoming
// Once the incoming message is noted, send another request to mark the Notify
// File for the person in focus, to false so that you have a clean state to
// Report (new) incoming messages accordingly
function ResetNotify(receiver) {
	var requestForm = document.createElement("form");
	var changeNotifyStatusToFalse = document.createElement("input");
	changeNotifyStatusToFalse.setAttribute("type", "text");
	changeNotifyStatusToFalse.setAttribute("name", "receiverPathRoot");
	changeNotifyStatusToFalse.setAttribute("value", receiver);
	requestForm.append(changeNotifyStatusToFalse);

	const request = new XMLHttpRequest();
	request.open("POST", "ResetNotify.php");
	request.onload = function() { 
		// Later ..
	}
	request.send(new FormData(requestForm));
}

document.querySelector("#recipient").addEventListener("mouseover", function(){
    let sender = document.querySelector("#user").innerHTML;
    document.querySelector(".fileStatisticsContainerForReceiver").style.display = "block";
    // (Graph) 
    GetStatistics(sender, this);
	
	let friends = document.querySelector(".friends").children;
	for(var i = 0; i < friends.length; ++i) {
	    let friend = friends[i].children;
	    if(this.innerHTML === friend[1].innerHTML) {
	        document.querySelector("#rRange").value = friend[2].value;
	        var memoryUsagePercentage = Math.ceil((friend[2].value / 50) * 100);
	        document.querySelector("#memoryUsagePercentage").innerHTML = `${memoryUsagePercentage}/50 MB`;
	        return;
	    }
	}
}, false);

document.querySelector("#recipient").addEventListener("mouseout", function(){
   document.querySelector(".fileStatisticsContainerForReceiver").style.display = "none";
}, false);
document.querySelector("#contacts").addEventListener("mouseover", requestRec, false);