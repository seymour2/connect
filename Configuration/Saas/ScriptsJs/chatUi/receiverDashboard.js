function ReceiverDashboard() {
	document.querySelector(".receiverDashboard").style.display = "block";
	// Pin the first three friends
	setFriends();
}

// Pin the first three friends from the friends pane to the dashboard pane
// This will allow quick traversal / swapping of a friend from the header
// The receiver dashboard also keeps track of last sent 3 message
function setFriends() {
	let friends = document.querySelector(".friends").children;
	let pins = document.querySelector(".colorSelection").children;
	let numberTimesToLoop;
	let friendsNotPinned;
	if (friends.length <= 3 && friends.length > 0) {
		numberTimesToLoop = friends.length;
		document.querySelector("#numberOfFriendsLeft").innerHTML = `+${friends.length}`;
	} else {
		numberTimesToLoop = 3;
		friendsNotPinned = (friends.length - 3);
		document.querySelector("#numberOfFriendsLeft").innerHTML = `+${friendsNotPinned}`;
	}
	
	for(var i = 0; i < numberTimesToLoop; ++i) {
		let friend = friends[i].children;
		pins[i].innerHTML = friend[0].innerHTML // First letter of name
		pins[i].style.backgroundColor = friend[0].style.backgroundColor;
		pins[i].style.color = "black";
		pins[i].style.fontFamily = "JetBrains Mono";
		pins[i].style.fontSize = "16px";
	}
}

// Enables the user to change which friend to message from the header
// Keep in mind that the children for a friend can be 4 depending on
// Unread status -- when parsed
function SwapFriend() {
	let pins = document.querySelector(".colorSelection").children;

	for(var i = 0; i < 3; ++i) {
		pins[i].onclick = function() {
			// Get the letter for receiver and use that
			// Same letter to search through list of friends
			// So that you can change the next receipient on match
			let pinInfo = this.innerHTML; 
			// Set the friend initials in the text box
			document.querySelector("#receiverDashboard").style.backgroundColor = this.style.backgroundColor;
			document.querySelector("#receiverDashboard").innerHTML = this.innerHTML

			let receivers = document.querySelector(".friends").children;
			// Find the friend from the friends pane and use thier
			// Information to update the name and picture for the next
			// Recipient
			for(var j = 0; j < 3; ++j) {
				// Get all the attributes for a friend
				// receiver[0]: initials
				// receiver[1]: name
				// receiver[2]: rangeBar -- memory usage
				if (receivers[j]) {
					let receiver = receivers[j].children;
					if (pinInfo == receiver[0].innerHTML) {
						// Hide the unread message status
						if(receiver.length == 4) {
							receiver[3].style.display = "none";
						}
					// Send a request to get the profile picture
					const requestProfilePicture = new XMLHttpRequest();
					requestProfilePicture.open("POST", `../../${receiver[1].innerHTML}/Init/Screenshot`);
					
					requestProfilePicture.onload = function() {
						if(this.readyState == 4 && this.status == 200) {
							var rPicture = `../../${receiver[1].innerHTML}/Init/${this.response}`;
							document.querySelector("#rPicture").src = rPicture;
							document.querySelector("#recipient").innerHTML = receiver[1].innerHTML;
							document.querySelector("#receiverDashboardName").innerHTML = receiver[1].innerHTML;
							document.querySelector("#receiverDashImage").src = rPicture;

							// Send another request to get the message content
							const requestMessageContent = new XMLHttpRequest();
							requestMessageContent.open("POST", `../Friends/${receiver[1].innerHTML}/Messages/messages`);
							
							requestMessageContent.onload = function() {
								if(this.readyState == 4 && this.status == 200) {
									document.querySelector(".contentUi").innerHTML = this.response;
								}
							}
							requestMessageContent.send();
						}
					}
					requestProfilePicture.send();
				}
				}
			}
		}
	}
}
document.querySelector(".colorSelection").addEventListener("mouseover", SwapFriend, false);
document.querySelector("#receiverDashboard").addEventListener("click", ReceiverDashboard, false);
document.querySelector("#BubbleSelectorOFF").addEventListener("click",function() {
	document.querySelector(".receiverDashboard").style.display = "none";
}, false);
// document.querySelector("#OFFBubble").addEventListener("click", HideReceiverDashboard, false);