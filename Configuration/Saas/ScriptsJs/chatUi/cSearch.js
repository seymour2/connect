// Search through (contact) list of friends
function cSearch() {
	var contacts = document.querySelector("#contacts").children;
	var search = document.querySelector("#cSearch").value.toUpperCase();

	for (var i = 0; i < contacts.length; i++) {
		var elements = contacts[i].children;
		var contact = elements[1].innerHTML;

		// Search
		if (contact.toUpperCase().indexOf(search) > -1) {
			contacts[i].style.display = "flex";
		} else {
			contacts[i].style.display = "none";
		}
	}
}