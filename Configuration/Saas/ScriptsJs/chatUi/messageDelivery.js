function appendMessageToContentUi() {
	var time = new Date();

	// Get (parse) the contentUi id for appending cnt: content 
	var user = document.querySelector("#user").innerHTML;
	var contentUi = document.querySelector("#sharedMsg");
	var message = document.querySelector("#messagecnt").value;

	// Create the HTML elements
	var messageFrame = document.createElement("div");
	var messageContent = document.createElement("div");
	var firstLetterOfName = document.createElement("p");
	var nameOfSender = document.createElement("h3");
	var messageToSend = document.createElement("p");

	// Set the respective class attributes
	messageFrame.setAttribute("class", "messageFrame");
	messageContent.setAttribute("class", "messageContent");
	firstLetterOfName.setAttribute("class", "border");

	// Set the content
	firstLetterOfName.innerHTML = user[0];
	nameOfSender.innerHTML = user;
	messageToSend.innerHTML = message;

	// Append and parse the elements for the message
	messageContent.append(nameOfSender);
	messageContent.append(messageToSend);
	messageFrame.append(firstLetterOfName);
	messageFrame.append(messageContent);

	// Add to the list of messages
	contentUi.append(messageFrame);

	// Play delivery tone --Short.mp3
	document.querySelector("#messageDeliveryTone").play();
}

// Send message to messageUi.php
function sendToMessageUiPhp() {
	appendMessageToContentUi();

	// Sroll to focus on last message sent
	var focusLastMessageSent = document.querySelector(".contentUi").children;
	if(focusLastMessageSent.length != 0) {
		focusLastMessageSent[focusLastMessageSent.length - 1].scrollIntoView();
	}

	var sender = document.querySelector("#user").innerHTML;
	var message = document.querySelector("#messagecnt").value;
	var recipient = document.querySelector("#recipient").innerHTML;

	// Setup the form inputs to send message info
	var form = document.createElement("form");

	var rRecipient = document.createElement("input");
	rRecipient.setAttribute("type", "text");
	rRecipient.setAttribute("name", "recipient");
	rRecipient.setAttribute("value", recipient);

	var sSender = document.createElement("input");
	sSender.setAttribute("type", "text");
	sSender.setAttribute("name", "sender");
	sSender.setAttribute("value", sender);

	var mMessage = document.createElement("input");
	mMessage.setAttribute("type", "text");
	mMessage.setAttribute("name", "message");
	mMessage.setAttribute("value", message);

	// Parse (append) the input elements to the form
	form.append(rRecipient);
	form.append(sSender);
	form.append(mMessage);

	var formData = new FormData(form);

	// Setup (ajax) HttpRequest to send form
	const request = new XMLHttpRequest();
	request.open("POST", "messageUi.php", true);

	request.onload = function() {
		//alert(this.response);
		if (this.readyState == 4 && this.status == 400) {
			alert("internal error");
		}
	}
	request.send(formData);
	document.querySelector("#messagecnt").value = "";
	document.querySelector("#wordCount").innerHTML = "+0";
}

// update and show word count
document.querySelector("#messagecnt").addEventListener("keyup", function(){
    document.querySelector("#wordCount").innerHTML = `+${this.value.length}`;
}, true);


// Deliver the message when the enter key is pressed
// src: https://www.codegrepper.com/code-examples/javascript/addeventlistener+for+enter+key
const keyboard = document.getElementById("messagecnt");

keyboard.addEventListener("keypress", (event)=> {
    if (event.keyCode === 13) { // key code of the keybord key
      event.preventDefault();
	 // your code to Run
	 sendToMessageUiPhp();
    }
  });