function UpdateMessageHeaderAndCacheForLater() {
	var bgColor = document.querySelector("#bgColor").value;
	document.querySelector(".headerUi").style.backgroundColor = bgColor;

	// Create a form with the changed color to cache on startUp
	var colorSchemeCacheForm = document.createElement("form");

	var cachedColor = document.createElement("input");
	cachedColor.setAttribute("type", "text");
	cachedColor.setAttribute("name", "cachedColor");
	cachedColor.setAttribute("value", bgColor);

	var cachedColorType = document.createElement("input");
	cachedColorType.setAttribute("type", "text");
	cachedColorType.setAttribute("name", "cachedColorType");
	cachedColorType.setAttribute("value", "header");

	// Parse the cache input to the form
	colorSchemeCacheForm.append(cachedColor);
	colorSchemeCacheForm.append(cachedColorType);
	
	var colorSchemeCacheFormRequest = new FormData(colorSchemeCacheForm);

	// SetUp ajax HttpRequest to send form to server
	const cacheRequest = new XMLHttpRequest();
	cacheRequest.open("POST", "colorSchemePaletteCache.php", true);

	cacheRequest.onload = function() {
		if (this.readyState == 4 && this.status == 400) {
			alert("Error caching color");
		} else { alert(this.response); }
	}
	cacheRequest.send(colorSchemeCacheFormRequest);
}

// Focus the last message sent on page load
var focusLastMessageSent = document.querySelector(".contentUi").children;
if (focusLastMessageSent.length > 3) {
    focusLastMessageSent[focusLastMessageSent.length - 1].scrollIntoView();
}

document.querySelector("#bgColor").addEventListener("change", UpdateMessageHeaderAndCacheForLater, false);