function CheckUsernameCreation() {
	var username = document.querySelector("#userId").value;
	document.querySelector("#userInitials").innerHTML = username[0];
	
	//Username Data
	var usernameForm = document.createElement("form");

	var usernameInputData = document.createElement("input");
	usernameInputData.setAttribute("type", "text");
	usernameInputData.setAttribute("name", "username");
	usernameInputData.setAttribute("value", username);

	usernameForm.append(usernameInputData);
	var formData = new FormData(usernameForm);

	// Request username status
	const request = new XMLHttpRequest();
	request.open("POST", "SaaS/checkAccountExistence.php");
	request.onload = function() {
		// Status of username requested
		if (this.response == "Username available") {
			document.querySelector("#userId").style.color = "green";
		} else {
			document.querySelector("#userId").style.color = "red";
		}
	}
	request.send(formData);
}

function RegisterSession() {
	var username = Caps(document.querySelector("#userId").value);
	var passcode = document.querySelector("#key").value;

	const request = new XMLHttpRequest();

	request.open("POST", "SaaS/startUserSession.php", true);
	request.onload = function() {
		if(this.readyState === 4 && this.status === 200) {
			switch(this.response) {
				case "false": 
					alert("Error finding user");
					break;

				case `${username}`: 
					window.location.href = `http://localhost/connect/Accounts/Members/${this.response}/Saas/main.php`;
					break;

				default:
					alert("Unable to process user request");
			}
		}
	}

	request.send(new FormData(document.querySelector("#formIdForUserRequest")));
}

function Caps(toCapitalize) {
	toCapitalize = toCapitalize.split(" ");

	for (let i = 0; i < toCapitalize.length; i++) {
	    toCapitalize[i] = toCapitalize[i][0].toUpperCase() + toCapitalize[i].substr(1);
	}
	return toCapitalize.join(" ");
}
document.querySelector("#userId").addEventListener("change", CheckUsernameCreation, false);
//document.querySelector("#create").addEventListener("click", RegisterSession, true);
document.querySelector("#bgColor").addEventListener("change",function(){
    document.querySelector("#userInitials").style.backgroundColor = this.value;
},false);
