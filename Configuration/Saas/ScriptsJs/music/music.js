var nextSongToPlay = -1;
var nextUpdatedSongLink = -1;
var nextArtistToPlay = -1;

function StartAnotherSong() {
	const artist = [
	    "Electro-Light & Protoson - <b>Pixel Dreams</b>",
		"Jim Yosef, Electro-Light, Anna Yvette, Deaf Kev & Tobu - <b>Destiny</b>",
		"AURORA - <b>Run Away</b>",
		"Michael Jackson - <b>Speechless</b>",
		"Mappe Of - <b>Dead Letter</b>"
	];

	const songs = [
	    "../../../../Configuration/Records/audio/Electro-Light & ProtosoniX - Pixel Dreams.mp4",
		"../../../../Configuration/Records/audio/Jim Yosef, Electro-Light, Anna Yvette, Deaf Kev & Tobu - Destiny.mp4",
		"../../../../Configuration/Records/audio/AURORA - Runaway.mp4",
		"../../../../Configuration/Records/audio/Michael Jackson - Speechless.mp4",
		"../../../../Configuration/Records/audio/Wintersleep - Dead Letter.mp4"
	];

	const links = [
	    "https://www.youtube.com/watch?v=EsG6M0AP5bk",
		"https://www.youtube.com/watch?v=bLl07iiLJ2c",
		"https://www.aurora-music.com/",
		"https://www.michaeljackson.com/",
		"https://www.mappeof.com/"
	];

	if (nextSongToPlay == (songs.length - 1)) {
		nextSongToPlay = 0;
		nextArtistToPlay = 0;
		nextUpdatedSongLink = 0;
		// Start over song selection
		// Update the song Queue ... 
	} else { 
		// Traverse to the next Queue Song
		nextSongToPlay++; 
		nextArtistToPlay++;
		nextUpdatedSongLink++;
	}

	var nextUpdatedMusicLink = `<a href="${links[nextUpdatedSongLink]}" target="_blank">${artist[nextArtistToPlay]}</a>`;
	document.querySelector("#musicVideo").src = songs[nextSongToPlay];
	document.querySelector("#musicVideo").play();
	document.querySelector("#musicNameLink").innerHTML = nextUpdatedMusicLink;
}

function StartMusicVideo() {
	document.querySelector(".music").style.display = "block";
}

document.querySelector("#MusicVideos").addEventListener("click", StartMusicVideo, false);
document.querySelector("#musicVideo").addEventListener("ended", StartAnotherSong, false);
document.querySelector("#OFFBubble").addEventListener("click", function(){
	document.querySelector(".music").style.display = "none";
}, false);