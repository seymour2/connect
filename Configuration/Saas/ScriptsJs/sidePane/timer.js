// Update the time: in Pacific Daylight Time
const timerElement = document.querySelector("#timer");
setInterval(
	function () {
		const tme = new Date();
		timerElement.innerHTML = tme.toLocaleString('us-OR', { timeZone: 'PST',hour12:false });
  }, 1000
);

var receiver = document.querySelector("#user");
    // Graph 
	var xValues = ["January", "Februrary", "March", "April", "May"];
	var yValues = [20, 25, 30, 25, 6];
	var barColors = [
	  "#b91d47",
	  "#00aba9",
	  "#2b5797",
	  "#e8c3b9",
	  "#1e7145"
	];

	new Chart("CacheChart", {
	  type: "bar",
	  data: {
	    labels: xValues,
	    datasets: [{
	      label: "Cache",
	      borderColor: "black",
	      backgroundColor: barColors,
	      data: yValues
	    }]
	  },
	  options: {
	    title: {
	      display: true,
	      text: receiver.innerHTML,
	      fontSize: "20",
	      fontFamily: "Plus Jakarta Sans"
	    }
	  }
	});

