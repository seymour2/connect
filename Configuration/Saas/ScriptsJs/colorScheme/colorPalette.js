function FPaneBgColor() {
	var cacheColor = document.querySelector("#fPaneBgColor").value;
	document.querySelector(".friendsUi").style.backgroundColor = cacheColor;

	// Create a form with the changed cacheColor to cache on startUp
	var colorSchemeCacheForm = document.createElement("form");

	var cachedColor = document.createElement("input");
	cachedColor.setAttribute("type", "text");
	cachedColor.setAttribute("name", "cachedColor");
	cachedColor.setAttribute("value", cacheColor);

	var cachedColorType = document.createElement("input");
	cachedColorType.setAttribute("type", "text");
	cachedColorType.setAttribute("name", "cachedColorType");
	cachedColorType.setAttribute("value", "fPaneBgColor");

	// Parse the cache input to the form
	colorSchemeCacheForm.append(cachedColor);
	colorSchemeCacheForm.append(cachedColorType);
	
	var colorSchemeCacheFormRequest = new FormData(colorSchemeCacheForm);

	// SetUp ajax HttpRequest to send form to server
	const cacheRequest = new XMLHttpRequest();
	cacheRequest.open("POST", "colorSchemePaletteCache.php", true);

	cacheRequest.onload = function() {
		if (this.readyState == 4 && this.status == 400) {
			alert("Error caching cacheColor");
		} else { alert(this.response); }
	}
	cacheRequest.send(colorSchemeCacheFormRequest);
}

function FontColorForMessageBox() {
	var cacheColor = document.querySelector("#messageBoxFontColor").value;
	document.querySelector("#messagecnt").style.color = cacheColor;
}

function SearchBarBgColor() {
	var cacheColor = document.querySelector("#searchBarBgColor").value;
	document.querySelector(".searchBar").style.backgroundColor = cacheColor;

	// Create a form with the changed cacheColor to cache on startUp
	var colorSchemeCacheForm = document.createElement("form");

	var cachedColor = document.createElement("input");
	cachedColor.setAttribute("type", "text");
	cachedColor.setAttribute("name", "cachedColor");
	cachedColor.setAttribute("value", cacheColor);

	var cachedColorType = document.createElement("input");
	cachedColorType.setAttribute("type", "text");
	cachedColorType.setAttribute("name", "cachedColorType");
	cachedColorType.setAttribute("value", "searchBar");

	// Parse the cache input to the form
	colorSchemeCacheForm.append(cachedColor);
	colorSchemeCacheForm.append(cachedColorType);
	
	var colorSchemeCacheFormRequest = new FormData(colorSchemeCacheForm);

	// SetUp ajax HttpRequest to send form to server
	const cacheRequest = new XMLHttpRequest();
	cacheRequest.open("POST", "colorSchemePaletteCache.php", true);

	cacheRequest.onload = function() {
		if (this.readyState == 4 && this.status == 400) {
			alert("Error caching cacheColor");
		} else { alert(this.response); }
	}
	cacheRequest.send(colorSchemeCacheFormRequest);
}

function MessageBoxBGColor() {
	var cacheColor = document.querySelector("#messageBox").value;
	document.querySelector(".sendUi").style.backgroundColor = cacheColor;

	// Create a form with the changed cacheColor to cache on startUp
	var colorSchemeCacheForm = document.createElement("form");

	var cachedColor = document.createElement("input");
	cachedColor.setAttribute("type", "text");
	cachedColor.setAttribute("name", "cachedColor");
	cachedColor.setAttribute("value", cacheColor);

	var cachedColorType = document.createElement("input");
	cachedColorType.setAttribute("type", "text");
	cachedColorType.setAttribute("name", "cachedColorType");
	cachedColorType.setAttribute("value", "messageBox");

	// Parse the cache input to the form
	colorSchemeCacheForm.append(cachedColor);
	colorSchemeCacheForm.append(cachedColorType);
	
	var colorSchemeCacheFormRequest = new FormData(colorSchemeCacheForm);

	// SetUp ajax HttpRequest to send form to server
	const cacheRequest = new XMLHttpRequest();
	cacheRequest.open("POST", "colorSchemePaletteCache.php", true);

	cacheRequest.onload = function() {
		if (this.readyState == 4 && this.status == 400) {
			alert("Error caching cacheColor");
		} else { alert(this.response); }
	}
	cacheRequest.send(colorSchemeCacheFormRequest);
}

function CacheColorRequest(cachedColor, cachedColorGuid) {
	// Create a form with the changed cacheColor to cache on startUp
	var colorSchemeCacheForm = document.createElement("form");

	var cachedColor = document.createElement("input");
	cachedColor.setAttribute("type", "text");
	cachedColor.setAttribute("name", "cachedColor");
	cachedColor.setAttribute("value", cachedColor);

	var cachedColorType = document.createElement("input");
	cachedColorType.setAttribute("type", "text");
	cachedColorType.setAttribute("name", "cachedColorType");
	cachedColorType.setAttribute("value", cachedColorGuid);

	// Parse the cache input to the form
	colorSchemeCacheForm.append(cachedColor);
	colorSchemeCacheForm.append(cachedColorType);
	
	var colorSchemeCacheFormRequest = new FormData(colorSchemeCacheForm);

	// SetUp ajax HttpRequest to send form to server
	const cacheRequest = new XMLHttpRequest();
	cacheRequest.open("POST", "colorSchemePaletteCache.php", true);

	cacheRequest.onload = function() {
		if (this.readyState == 4 && this.status == 400) {
			alert("Error caching cacheColor");
		} else { alert(this.response); }
	}
	cacheRequest.send(colorSchemeCacheFormRequest);
}

// Get the color scheme for the friends pane
function GetColorScheme() {
	const requestColorScheme = new XMLHttpRequest();
	requestColorScheme.open("POST","../Init/ColorSchemePalette/friendsPaneColorScheme");

	requestColorScheme.onload = function() {
		document.querySelector(".friendsUi").style.backgroundColor = this.response;
	}
	requestColorScheme.send();
}
document.querySelector("#fPaneBgColor").addEventListener("change", FPaneBgColor);
document.querySelector("#messageBox").addEventListener("change", MessageBoxBGColor);
document.querySelector("#searchBarBgColor").addEventListener("change", SearchBarBgColor);
document.querySelector("#messageBoxFontColor").addEventListener("change", FontColorForMessageBox);