// src: https://www.geeksforgeeks.org/how-to-convert-speech-into-text-using-javascript/
		var speech = true;

	// Start the recording after recordingMessage frame is constructed
        function StartSpeechRecording() {
            window.SpeechRecognition = window.SpeechRecognition  || window.webkitSpeechRecognition;
                           

            const recognition = new SpeechRecognition();
            recognition.interimResults = true;
            const messageHistory = document.querySelector('.contentUi');
            var tempMessage;

            // Create the HTML elements
			var messageFrame = document.createElement("div");
			var messageContent = document.createElement("div");
			var firstLetterOfName = document.createElement("p");
			var nameOfSender = document.createElement("h3");
			var messageToSend = document.createElement("p");

			// Set the respective class attributes
			messageFrame.setAttribute("class", "messageFrame");
			messageContent.setAttribute("class", "messageContent");
			firstLetterOfName.setAttribute("class", "border");

			// Set the content
			firstLetterOfName.innerHTML = document.querySelector("#recipient").innerHTML[0];
			nameOfSender.innerHTML = document.querySelector("#user").innerHTML;
			//messageToSend.innerHTML = document.querySelector("#messagecnt").value;

			// Append and parse the elements for the message
			messageContent.append(nameOfSender);
			messageContent.append(messageToSend);
			messageFrame.append(firstLetterOfName);
			messageFrame.append(messageContent);

			// Add to the list of messages
			//messageHistory.append(messageFrame);
			
            // Start the api --recording
            recognition.addEventListener('result', e => {
                var transcript = Array.from(e.results)
                    .map(result => result[0])
                    .map(result => result.transcript)
                    .join('')
                   
                messageToSend.innerHTML = transcript;
                tempMessage = transcript;
                messageHistory.appendChild(messageFrame);
            });

            if (speech == true) {
                recognition.start();
                recognition.addEventListener('end', ()=>{
                	recognition.stop;
                	DeliverRecordedMessage(tempMessage);
                });
            }
        }
  // Deliver the recorded message
  function DeliverRecordedMessage(message) {
  	// Sroll to focus on last message sent
	var focusLastMessageSent = document.querySelector(".contentUi").children;
	if(focusLastMessageSent.length != 0) {
		focusLastMessageSent[focusLastMessageSent.length - 1].scrollIntoView();
	}

	var sender = document.querySelector("#user").innerHTML;
	var recipient = document.querySelector("#recipient").innerHTML;

	// Setup the form inputs to send message info
	var form = document.createElement("form");

	var rRecipient = document.createElement("input");
	rRecipient.setAttribute("type", "text");
	rRecipient.setAttribute("name", "recipient");
	rRecipient.setAttribute("value", recipient);

	var sSender = document.createElement("input");
	sSender.setAttribute("type", "text");
	sSender.setAttribute("name", "sender");
	sSender.setAttribute("value", sender);

	var mMessage = document.createElement("input");
	mMessage.setAttribute("type", "text");
	mMessage.setAttribute("name", "message");
	mMessage.setAttribute("value", message);

	// Parse (append) the input elements to the form
	form.append(rRecipient);
	form.append(sSender);
	form.append(mMessage);

	var formData = new FormData(form);

	// Setup (ajax) HttpRequest to send form
	const request = new XMLHttpRequest();
	request.open("POST", "messageUi.php", true);

	request.onload = function() {
		//alert(this.response);
		if (this.readyState == 4 && this.status == 400) {
			alert("internal error");
		} else if (this.readyState == 4 && this.status == 200) {
			// Later ... 
		} else {
			alert("Server unable to process request");
		}
	}
	request.send(formData);
  }      

document.querySelector("#microphone").addEventListener("click", StartSpeechRecording);